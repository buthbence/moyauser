//
//  UserService.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import Foundation
import Moya

enum UserService { // These are services what I can do with a user.
    
    case readUsers
}

extension UserService: TargetType {
    var baseURL: URL {
        return URL(string: "https://randomuser.me/api/?page=\(Link.page)&results=10&seed=\(Link.seed)")!
    }
    var path: String {
        switch self {
        case .readUsers:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .readUsers:
            return .get
        }
    }
    
    var sampleData: Data { // I need if i want to do Unit tests...
        return Data()
    }
    
    var task: Task { //  how i pass my parameters through my API
        switch self {
            
            
        case .readUsers:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/json"]
    }
    
    
}

