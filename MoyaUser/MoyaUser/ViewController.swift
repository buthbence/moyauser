//
//  ViewController.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import UIKit
import Moya


class ViewController: UITableViewController {
    
    var userDefaults = UserDefaults.standard //send user to the another controller to show its details
    var users = [User]() // store users in it
    let userProvider = MoyaProvider<UserService>() //moya service
    lazy var refresher: UIRefreshControl = { //refresh the users on the main page
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .blue
        refreshControl.addTarget(self, action: #selector(generateRandomString), for: .valueChanged)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        requestData()
        tableView.refreshControl = refresher
        
    }
    
    @objc func generateRandomString () { // pull to request
        Link.seed = self.randomString(length: 4) // generate new seed to pull to request
        self.requestData()
        let deadLine = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadLine) {
            self.refresher.endRefreshing()
        }
    }
    
    func requestData () {
        userProvider.request(.readUsers) { (result) in
                self.users.removeAll()
                switch result {
                case .success(let response):
                    let json = try! JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any]
                    for userJSON in json!.array ("results") {
                        if let user = User(json: userJSON) {
                            self.users.append(user)
                        }
                    }
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func randomString(length: Int) -> String { //generate random string to seed
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    
    @IBAction func goPreviousPage(_ sender: Any) { // go to previous page
            if Link.page > 1 {
                Link.page -= 1
                self.requestData()
            }

    }
    
    @IBAction func goNextPage(_ sender: Any) { // go to next page
            Link.page += 1
            self.requestData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let user  = users[indexPath.row]
        let url = user.thumbnail
        let data = try? Data(contentsOf: url)
        cell.imageView?.image = UIImage(data: data!)
        cell.textLabel?.text = "\(user.first.capitalizingFirstLetter()) \(user.last.capitalizingFirstLetter())"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = users[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "user", sender: user)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // for segue.
        let vc = segue.destination as! DetailsViewController
        if segue.identifier == "user" {
            vc.user = (sender as! User)
        }
    }    
}


