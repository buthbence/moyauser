//
//  User.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import Foundation

struct User : Codable {

    var first : String
    var last : String
    var thumbnail: URL
    var gender: String
    var location: String
    var email: String
    var phone: String
    var picture: URL
    
    init?(json: [String: Any]) {
        
        guard let name = json.dict("name") else { return nil }
        guard let first = name.string("first") else { return nil }
        guard let last = name.string("last") else { return nil }
        
        guard let thumbnail = json.dict("picture")?.string("thumbnail") else { return nil }
        guard let thumbnailURL = URL(string: thumbnail) else { return nil }
        
        guard let picture = json.dict("picture")?.string("large") else { return nil }
        guard let pictureURL = URL(string: picture) else { return nil }
        
        guard let gender = json.string("gender") else {return nil}
        
        guard let location = json.dict("location") else {return nil}
        guard let city = location.string("city") else {return nil}
        
        guard let email = json.string("email") else {return nil}
        
        guard let phone  = json.string("phone") else {return nil}
        
        self.first = first
        self.last = last
        self.thumbnail = thumbnailURL
        self.gender = gender
        self.location = city
        self.email = email
        self.phone = phone
        self.picture = pictureURL
    }
}

