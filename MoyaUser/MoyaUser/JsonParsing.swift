//
//  JsonParsing.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import Foundation

typealias JSONDict = [String: Any]

extension Dictionary where Key == String {
    func string(_ key: String) -> String? {
        if let value = self[key] as? String {
            return value
        } else {
            print("Missing parameter '\(key)'")
            return nil
        }
    }
    
    func array(_ key: String) -> [JSONDict] {
        if let value = self[key] as? [JSONDict] {
            return value
        } else {
            print("Missing parameter '\(key)'")
            return []
        }
    }
    
    func dict(_ key: String) -> JSONDict? {
        if let value = self[key] as? JSONDict{
            return value
        } else {
            print("Missing parameter '\(key)'")
            return nil
        }
    }
}
