//
//  File.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import Foundation
class User { //it was decodable
    //let id: String
    var first : String //let volt az osszes eredetileg
    var last : String
    var thumbnail: URL
    var gender: String
    var location: String
    var email: String
    var phone: String
}
