//
//  DetailsViewController.swift
//  MoyaUser
//
//  Created by Buth Bence on 2019. 01. 17..
//  Copyright © 2019. BBence. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    let detailsContainerView: UIView = { //subview of view which is contain textfields
        let cv = UIView()
        cv.backgroundColor = UIColor.white
        cv.layer.cornerRadius = 5
        cv.layer.masksToBounds = true // if we dont do it the corner radius wont shown.
        cv.translatesAutoresizingMaskIntoConstraints = false
        
        return cv
    }()
    
    let nameAndImageContainerView: UIView = { //subview of view which is contain textfields
        let cv = UIView()
        cv.backgroundColor = UIColor.white
        cv.layer.cornerRadius = 5
        cv.layer.masksToBounds = true // if we dont do it the corner radius wont shown.
        cv.translatesAutoresizingMaskIntoConstraints = false
        
        return cv
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    //register and login button
    var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        let borderAlpha : CGFloat = 0.7
        button.layer.borderWidth = 1.5
        button.layer.borderColor = UIColor.black.cgColor
        button.backgroundColor = UIColor.clear
        button.setTitle("Cancel", for: .normal)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        
        return button
    }()
    
    let emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.justified
        return label
    }()
    
    let phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.justified
        return label
    }()
    
    let genderLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.justified
        return label
    }()
    
    let cityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.justified
        return label
    }()
    
    let profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    var user: User?
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(detailsContainerView)
        view.addSubview(nameAndImageContainerView)
        setupdetailsContainerView()
        setupNameAndImageContainerView()
        
        nameLabel.text = "\(user!.first.capitalizingFirstLetter()) \(user!.last.capitalizingFirstLetter())"
        emailLabel.text = "Email: \(user!.email)"
        phoneLabel.text = "Phone: \(user!.phone)"
        genderLabel.text = "Gender: \(user!.gender.capitalizingFirstLetter())"
        cityLabel.text = "City: \(user!.location.capitalizingFirstLetter())"
        let url = user!.picture
        let data = try? Data(contentsOf: url)
        profileImage.image = UIImage(data: data!)
    }
    
    
    @objc func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    var detailsContainerViewHeight: NSLayoutConstraint?
    var emailLabelHeight: NSLayoutConstraint?
    var phoneLabelHeight: NSLayoutConstraint?
    var genderLabelHeight: NSLayoutConstraint?
    var cityLabelHeight: NSLayoutConstraint?
    
    func setupNameAndImageContainerView() {
        
        nameAndImageContainerView.addSubview(profileImage)
        nameAndImageContainerView.addSubview(nameLabel)
        
        nameAndImageContainerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        nameAndImageContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        nameAndImageContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        nameAndImageContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        //profileImage
        profileImage.centerXAnchor.constraint(equalTo: nameAndImageContainerView.centerXAnchor).isActive = true
        profileImage.centerYAnchor.constraint(equalTo: nameAndImageContainerView.centerYAnchor).isActive = true
        profileImage.heightAnchor.constraint(equalTo: nameAndImageContainerView.heightAnchor, multiplier: 0.4).isActive = true
        
        //name
        nameLabel.centerXAnchor.constraint(equalTo: nameAndImageContainerView.centerXAnchor).isActive = true
        nameLabel.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 15).isActive = true
        nameLabel.widthAnchor.constraint(equalTo: nameAndImageContainerView.widthAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    //Setup the constraints of RegisterContainerView elements
    func setupdetailsContainerView() {
        
        detailsContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        detailsContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        detailsContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        detailsContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        detailsContainerView.addSubview(emailLabel)
        detailsContainerView.addSubview(phoneLabel)
        detailsContainerView.addSubview(genderLabel)
        detailsContainerView.addSubview(cityLabel)
        detailsContainerView.addSubview(cancelButton)
        
        //gender
        genderLabel.leftAnchor.constraint(equalTo: detailsContainerView.leftAnchor, constant: 12).isActive = true
        genderLabel.topAnchor.constraint(equalTo: detailsContainerView.topAnchor, constant: 20).isActive = true
        genderLabel.widthAnchor.constraint(equalTo: detailsContainerView.widthAnchor).isActive = true
        genderLabelHeight = genderLabel.heightAnchor.constraint(equalTo: detailsContainerView.heightAnchor, multiplier: 1/6)
        genderLabelHeight?.isActive = true
        
        //city
        cityLabel.leftAnchor.constraint(equalTo: detailsContainerView.leftAnchor, constant: 12).isActive = true
        cityLabel.topAnchor.constraint(equalTo: genderLabel.bottomAnchor).isActive = true
        cityLabel.widthAnchor.constraint(equalTo: detailsContainerView.widthAnchor).isActive = true
        cityLabelHeight = cityLabel.heightAnchor.constraint(equalTo: detailsContainerView.heightAnchor, multiplier: 1/6)
        cityLabelHeight?.isActive = true
        //email
        emailLabel.leftAnchor.constraint(equalTo: detailsContainerView.leftAnchor, constant: 12).isActive = true
        emailLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor).isActive = true
        emailLabel.widthAnchor.constraint(equalTo: detailsContainerView.widthAnchor).isActive = true
        emailLabelHeight = emailLabel.heightAnchor.constraint(equalTo: detailsContainerView.heightAnchor, multiplier: 1/6)
        emailLabelHeight?.isActive = true
        //Phone
        phoneLabel.leftAnchor.constraint(equalTo: detailsContainerView.leftAnchor, constant: 12).isActive = true
        phoneLabel.topAnchor.constraint(equalTo: emailLabel.bottomAnchor).isActive = true
        phoneLabel.widthAnchor.constraint(equalTo: detailsContainerView.widthAnchor).isActive = true
        phoneLabelHeight = phoneLabel.heightAnchor.constraint(equalTo: detailsContainerView.heightAnchor, multiplier: 1/6)
        phoneLabelHeight?.isActive = true
        //cancel
        cancelButton.centerXAnchor.constraint(equalTo: detailsContainerView.centerXAnchor).isActive = true
        cancelButton.topAnchor.constraint(equalTo: phoneLabel.bottomAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: detailsContainerView.widthAnchor, constant: -70).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
